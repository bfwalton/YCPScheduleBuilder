package com.bfwalton.YCPCourseBuilder.GUI.Menu;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

public class CourseListing<E> extends JPanel{
	private static final long serialVersionUID = 1L;
	
	public CourseListing(ArrayList<DataLine<E>> dataLines){
		this.setLayout(new GridLayout(dataLines.size(), 1));
		for(DataLine<E> line : dataLines){
			this.add(line);
		}
	}
}
