package com.bfwalton.YCPCourseBuilder.GUI.Menu;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.bfwalton.Course.Course;

public class Calendar extends JPanel{
	private static final long serialVersionUID = 1L;
	
	private ArrayList<DataLine<String>> lines = new ArrayList<DataLine<String>>();
	
	public Calendar(){
		
	}
	
	public void build(ArrayList<Course> courseList){
		for(int i = 0;i<24;i++){
			for(int j = 0;j<60;j+=15){
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(""+i+":"+j);
				lines.add(new DataLine<String>(temp));
			}
		}
		
		this.setLayout(new GridLayout(1,lines.size()));
		for(DataLine<String> line : lines){
			this.add(line);
		}
	}

}
