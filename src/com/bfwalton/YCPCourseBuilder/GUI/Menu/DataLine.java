package com.bfwalton.YCPCourseBuilder.GUI.Menu;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class DataLine<E> extends JPanel{
	private static final long serialVersionUID = 1L;
	private ArrayList<JLabel> labels;
	
	public DataLine(ArrayList<E> lineItems){
		init(lineItems);
		build();
	}
	
	public void init(ArrayList<E> lineItems){
		labels = new ArrayList<JLabel>();
		for(Object obj : lineItems){
			JLabel temp = new JLabel(obj.toString());
			temp.setBorder(BorderFactory.createLineBorder(Color.black));
			temp.setHorizontalAlignment(SwingConstants.CENTER);
			labels.add(temp);
		}
	}
	
	public JLabel getLabel(int index){
		return labels.get(index);
	}
	
	public void build(){
		this.setLayout(new GridLayout(1,labels.size()));
		for(JLabel label : labels){
			this.add(label);
		}
		
	}
}
