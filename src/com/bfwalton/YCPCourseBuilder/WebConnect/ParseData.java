package com.bfwalton.YCPCourseBuilder.WebConnect;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;


public abstract class ParseData {

	/**
	 * Gets all of the tags from the html code
	 * @param html
	 * @return an ArrayList of strings of code inside of the tags
	 */
	public static ArrayList<String> getAllTags(String html){
		//TODO: Write Method
		ArrayList<String> tagList = new ArrayList<String>();

		char[] htmlCharList = html.toCharArray();
		for(int i = 0;i<htmlCharList.length;i++){
			String tag = null;
			if(htmlCharList[i] == '<'){
				tag = Character.toString(htmlCharList[i]);
				i++;
				while(htmlCharList[i]!='>'){
					tag += Character.toString(htmlCharList[i]);
					i++;
				}
				tag+=">";
			}
			if(tag!=null)
				tagList.add(tag);
		}
		return tagList;
	}

	/**
	 * Sorts the html code into lines based on the tags
	 * @param html
	 * @return and ArrayList of lines of html code
	 */
	public static ArrayList<String> toLines(String html){
		ArrayList<String> lines = new ArrayList<String>();
		char[] htmlCharList = html.toCharArray();
		boolean inLine = false;
		String tempString = "";
		for(int i = 0;i<htmlCharList.length;i++){
			if(htmlCharList[i]=='>'){
				tempString+=htmlCharList[i];
				lines.add(tempString.toLowerCase());
				tempString = "";
				inLine = false;
			}
			else if(htmlCharList[i]=='<'){
				if(inLine == false && !tempString.equals("")){
					if(tempString.trim().length()>0)
						lines.add(tempString.trim().toLowerCase());
					tempString="";
				}
				tempString+=htmlCharList[i];
				inLine = true;
			}
			else{
				tempString += htmlCharList[i];
			}
		}
		return lines;
	}

	/**
	 * Removes the lines of HTML data from the text
	 * @param lines
	 * @return lines of text
	 */
	public static ArrayList<String> removeHTML(ArrayList<String> lines){
		ArrayList<String> temp = new ArrayList<String>();

		for(String str : lines){
			if(!str.contains("<")){
				temp.add(str);
			}
		}

		return temp;
	}

	/**
	 * Returns an ArrayList of strings of the specified HTML tag
	 * @param tags the tag to search for
	 * @return an arrayList of strings that are of the tag type
	 */
	public static ArrayList<String> getTags(String tag, ArrayList<String> lineList) throws Exception{
		ArrayList<String> lines = lineList;
		ArrayList<String> tempArray = new ArrayList<String>();
		String startText = "<"+tag;
		String endText = "</"+tag;

		boolean addLine = false;
		for(String line : lines){

			if(line.startsWith(startText)){
				addLine = true;
			}

			if(addLine){
				tempArray.add(line);
			}

			if(line.startsWith(endText)){
				addLine = false;
			}
		}

		return tempArray;
	}

	public static ArrayList<ArrayList<String>> getSeparateTags(String tag, ArrayList<String> lines){
		ArrayList<ArrayList<String>> rowListList = new ArrayList<ArrayList<String>>();
		boolean addLine = false;
		String startText = "<"+tag;
		String endText = "</"+tag;
		
		ArrayList<String> tempArray = null;
		for(String line : lines){
			if(line.startsWith(startText)){
				addLine = true;
				tempArray = new ArrayList<String>();
			}

			if(addLine){
				tempArray.add(line);
			}

			if(line.startsWith(endText)){
				addLine = false;
				rowListList.add(tempArray);
			}
		}

		return rowListList;
	}

	/**
	 * Returns the HTML code from the given url as a single String object
	 * @param url
	 * @return The html code
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static String getHTML(String url) throws MalformedURLException, IOException{
		String tempString = "";
		Scanner scan = new Scanner(new URL(url).openStream());
		while(scan.hasNextLine()){
			String line = scan.nextLine();
			tempString+=line;
		}
		scan.close();
		return tempString;
	}
}
