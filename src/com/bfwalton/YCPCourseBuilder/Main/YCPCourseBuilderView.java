package com.bfwalton.YCPCourseBuilder.Main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.bfwalton.Course.Course;
import com.bfwalton.Course.CourseBuilder;
import com.bfwalton.YCPCourseBuilder.GUI.Menu.CourseListing;
import com.bfwalton.YCPCourseBuilder.GUI.Menu.DataLine;

public class YCPCourseBuilderView extends JFrame{
	
	private static final long serialVersionUID = 1L;
	public static final String url = "http://ycpweb.ycp.edu/schedule-of-classes/index.html?term=201420&stype=A&dmode=D&dept=BIO_02";
	
	private static YCPCourseBuilderController controller;
	private static YCPCourseBuilderModel model;
	
	private JMenuBar bar;
	
	public YCPCourseBuilderView(){
		setModel(new YCPCourseBuilderModel());
		setController(new YCPCourseBuilderController());
	}
	
	public void build() throws MalformedURLException, IOException{
		bar = new JMenuBar();
		this.setJMenuBar(bar);
		this.setLayout(new BorderLayout());
		//this.setSize(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		//this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	
		ArrayList<DataLine<String>> dLine = new ArrayList<DataLine<String>>();
		
		ArrayList<String> titleArray = new ArrayList<String>();
		titleArray.add("CRN");
		titleArray.add("Course Title");
		titleArray.add("Days");
		titleArray.add("Times");
		titleArray.add("Credits");
		titleArray.add("Instructor");
		dLine.add(new DataLine<String>(titleArray));
		
		for(Course c : CourseBuilder.createCoursesFromUrl(url)){
			dLine.add(new DataLine<String>(c.getItemsAsArrayList()));
		}
		
		JScrollPane pane = new JScrollPane(new CourseListing<String>(dLine));
		pane.setPreferredSize(new Dimension(this.getWidth(), this.getHeight()));
		this.add(pane);
		
		this.setJMenuBar(bar);
		buildMenuBar();
		this.setVisible(true);
	}
	
	public void buildMenuBar(){
		bar.add(new JMenu("File"));
		bar.add(new JMenu("Subject"));
		bar.add(new JMenu("Sort By"));
		bar.add(new JMenu("Show only"));
		bar.add(new JMenu("View"));
		//bar.add(Box.createHorizontalStrut(Toolkit.getDefaultToolkit().getScreenSize().width/2));
		bar.add(Box.createHorizontalGlue());
		bar.add(Box.createHorizontalStrut(100));
		bar.add(new JMenu("Search For"));
		JTextField field = new JTextField();
		bar.add(field);
		bar.add(new JButton("Search"));
	}

	public static YCPCourseBuilderController getController() {
		return controller;
	}

	public static void setController(YCPCourseBuilderController controller) {
		YCPCourseBuilderView.controller = controller;
	}

	public static YCPCourseBuilderModel getModel() {
		return model;
	}

	public static void setModel(YCPCourseBuilderModel model) {
		YCPCourseBuilderView.model = model;
	}
	
	public static void main(String[] args){
		YCPCourseBuilderView view = new YCPCourseBuilderView();
		try {
			view.build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
