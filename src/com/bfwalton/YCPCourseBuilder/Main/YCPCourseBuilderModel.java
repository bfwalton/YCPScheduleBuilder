package com.bfwalton.YCPCourseBuilder.Main;

import java.util.ArrayList;
import java.util.Iterator;

import com.bfwalton.Course.Course;

public class YCPCourseBuilderModel {
	
	private ArrayList<Course> courseList = new ArrayList<Course>();
	
	public void addCourse(Course course){
		courseList.add(course);
	}
	
	public void removeCourse(Course course){
		courseList.remove(course);
	}
	
	public void removeCourse(int index){
		courseList.remove(index);
	}
	
	public int getNumberOfCourses(){
		return courseList.size();
	}
	
	public Course getCourse(int index){
		return courseList.get(index);
	}
	
	public Iterator<Course> getCourseList(){
		return courseList.iterator();
	}
}
