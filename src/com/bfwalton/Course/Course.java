package com.bfwalton.Course;

import java.util.ArrayList;

import com.bfwalton.YCPCourseBuilder.GUI.Menu.DataLine;

public class Course {

	private String crn;
	private String course;
	private String title;
	private String credits;
	private String type;
	private ArrayList<String> days = new ArrayList<String>();
	private ArrayList<SimpleTime> startTime = new ArrayList<SimpleTime>();
	private ArrayList<SimpleTime> endTime = new ArrayList<SimpleTime>();;
	private String location;
	private String instructor;
	private String seats;
	private String open;
	private String totEnrl;
	private String begin;
	private String end;

	public Course() {

	}
	public Course(String[] items){
		this.setCrn(items[0]);
		this.setCourse(items[1]);
		//2 is book info
		this.setTitle(items[3]);
		this.setCred(items[4]);
		this.setType(items[5]);
		this.addDays(items[6].replace("&nbsp;", " "));
		this.setStartTime(SimpleTime.parseSimpleTime(items[7].replace("&nbsp;", " ").replace("-", "")));
		this.setEndTime(SimpleTime.parseSimpleTime(items[8].replace("&nbsp;", " ").replace("-", "")));
		this.setLocation(items[9]);
		this.setInstructor(items[10]);
		this.setSeats(items[11]);
		this.setOpen(items[12]);
		this.setTot(items[13]);
		this.setBegin(items[14]);
		this.setEnd(items[15]);
	}

	public String toString() {
		return "(" + crn + ") " + course + " " + title + " " + credits + " "
				+ type + " " + " " + days + " " + startTime + " " + location + " " + " " + instructor + " " + seats + " " + open + " " 
				+ " " + totEnrl + " " + begin + " " + end;
	}

	public ArrayList<String> getItemsAsArrayList(){
		ArrayList<String> temp = new ArrayList<String>();

		temp.add(getCrn());
		temp.add(getTitle());
		String tempDayString = "";
		for(int i = 0; i<days.size();i++){
			tempDayString+=days.get(i);
			if(i!=days.size()-1){
				tempDayString+=" and ";
			}
		}
		temp.add(tempDayString);
		String tempTimeString = "";
		for(int i = 0; i<startTime.size();i++)
		{
			tempTimeString+=getStartTime(i)+"-"+getEndTime(i);
			if(i!=startTime.size()-1){
				tempTimeString+=" and ";
			}
		}
		temp.add(tempTimeString);
		temp.add(getCred());
		temp.add(getInstructor());

		return temp;
	}

	public DataLine<String> getDataLine(){
		return new DataLine<String>(getItemsAsArrayList());
	}

	public void printCourse(){
		System.out.println("CRN: "+getCrn());
		System.out.println("Course: "+getCourse());
		System.out.println("Title: "+getTitle());
		System.out.println("Credits: "+getCred());
		System.out.println("Type: "+getType());
		System.out.println("Days: "+getDays(0));
		System.out.println("Times: "+getStartTime(0)+" until "+getEndTime(0));
		if(startTime.size()>1){
			System.out.println("and "+getStartTime(1)+" until "+getEndTime(1));
		}
		System.out.println("Location: "+getLocation());
		System.out.println("Professor: "+getInstructor());
		System.out.println("Seats: "+getSeats()+", "+"Open seats: "+getOpen()+" Total enroll: "+getTot());
		System.out.println("Starts: "+getBegin()+" Ends: "+getEnd());

		System.out.println("-----------");
	}

	public String getCrn() {
		return crn.replace("&nbsp;", " ");
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getCourse() {
		return course.replace("&nbsp;", " ");
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getTitle() {
		return title.replace("&nbsp;", " ");
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCred() {
		return credits.replace("&nbsp;", " ");
	}

	public void setCred(String cred) {
		this.credits = cred;
	}

	public String getType() {
		return type.replace("&nbsp;", " ");
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDays(int index) {
		try{
			return days.get(index);
		}catch(IndexOutOfBoundsException e){
			return "No Days";
		}
	}

	public void addDays(String days) {
		parseDays(days);
	}

	public SimpleTime getStartTime(int index) {
		return startTime.get(index);
	}

	public SimpleTime getEndTime(int index){
		return endTime.get(index);
	}

	public void setStartTime(SimpleTime time) {
		this.startTime.add(time);
	}

	public void setEndTime(SimpleTime time) {
		this.endTime.add(time);
	}

	public String getLocation() {
		return location.replace("&nbsp;", " ");
	}

	public void setLocation(String loc) {
		this.location = loc;
	}

	public String getInstructor() {
		return instructor.replace("&nbsp;", " ");
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	public String getSeats() {
		return seats.replace("&nbsp;", " ");
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

	public String getOpen() {
		return open.replace("&nbsp;", " ");
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getTot() {
		return totEnrl.replace("&nbsp;", " ");
	}

	public void setTot(String tot) {
		this.totEnrl = tot;
	}

	public String getEnrl() {
		return totEnrl.replace("&nbsp;", " ");
	}

	public void setEnrl(String enrl) {
		this.totEnrl = enrl;
	}

	public String getBegin() {
		return begin.replace("&nbsp;", " ").replace("-", "");
	}

	public void setBegin(String begin) {
		this.begin = begin;
	}

	public String getEnd() {
		return end.replace("&nbsp;", " ");
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public void parseDays(String tempDays){
		tempDays = tempDays.toLowerCase();
		if(tempDays.contains("m")){
			if(!this.days.contains("Monday"))
				this.days.add("Monday");
		}
		if(tempDays.contains("t")){
			if(!this.days.contains("Tuesday"))
				this.days.add("Tuesday");
		}
		if(tempDays.contains("w")){
			if(!this.days.contains("Wednesday"))
				this.days.add("Wednesday");
		}
		if(tempDays.contains("r")){
			if(!this.days.contains("Thursday"))
				this.days.add("Thursday");
		}
		if(tempDays.contains("f")){
			if(!this.days.contains("Friday"))
				this.days.add("Friday");
		}
	}
	
	public boolean courseOverlaps(Course c){
		//Checks the time overlaps
		if(SimpleTime.timesOverlap(c.getStartTime(0), c.getEndTime(0), this.getStartTime(0), this.getEndTime(0))){
			for(int i = 0;i<c.getNumberOfDays();i++){
				if(days.contains(c.getDays(i))){
					return true;
				}
			}
		}
		return false;
	}
	private int getNumberOfDays() {
		return days.size();
	}
}
