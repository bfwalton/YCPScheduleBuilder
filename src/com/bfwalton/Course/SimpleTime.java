package com.bfwalton.Course;

public class SimpleTime {
	
	private int hours, minutes;
	
	public SimpleTime(){
		this.setHours(0);
		this.setMinutes(0);
	}
	
	public SimpleTime(int hours, int minutes){
		this.setHours(hours);
		this.setMinutes(minutes);
	}
	
	public static SimpleTime parseSimpleTime(String str){
		SimpleTime time = new SimpleTime();
		String[] items = str.split(":");
		
		if(items.length==2){
			time.setHours(Integer.parseInt(items[0]));
			
			if(items[1].contains("am")){
				time.setMinutes(Integer.parseInt(items[1].replace("am", "")));
			}
			else if(items[1].contains("pm")){
				time.setMinutes(Integer.parseInt(items[1].replace("pm", "")));
				time.setHours(time.getHours()+12);
			}
		}
		return time;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	
	public String get12HourString(){
		String ampm;
		int tempHours = hours;
		String strHours = "", strMinutes = "";
		if(String.valueOf(this.hours).length() == 1){
			strHours = "0";
		}
		if(String.valueOf(this.minutes).length() == 1){
			strMinutes = "0";
		}
		if(tempHours>12){
			ampm = "pm";
			tempHours-=12;
		}else{
			ampm = "am";
		}
		
		return strHours+tempHours+":"+strMinutes+minutes+ampm;
	}
	
	public int getTimeAsMinutes(){
		return minutes+(60*hours);
	}
	
	public String toString(){
		return get12HourString();// strHours+":"+strMinutes;
	}
	
	public static boolean timesOverlap(SimpleTime start1, SimpleTime end1, SimpleTime start2, SimpleTime end2){
		boolean returns = false;
		if(start1.getTimeAsMinutes()<start2.getTimeAsMinutes()){
			if(end1.getTimeAsMinutes()>start2.getTimeAsMinutes()){
				returns = true;
			}
		}
		
		if(start2.getTimeAsMinutes()<start1.getTimeAsMinutes()){
			if(end2.getTimeAsMinutes()>start1.getTimeAsMinutes()){
				returns = true;
			}
		}
		return returns;
	}
}
