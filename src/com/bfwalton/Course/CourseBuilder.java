package com.bfwalton.Course;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import com.bfwalton.YCPCourseBuilder.WebConnect.ParseData;

public class CourseBuilder {

	/**
	 * Uses the lines of raw HTML to generate the courses
	 * @param lines
	 * @return a list of courses contained by the lines
	 */
	public static ArrayList<Course> createCoursesFromRawData(ArrayList<String> lines){
		ArrayList<Course> courseList = new ArrayList<Course>();
		
		for(ArrayList<String> temp : ParseData.getSeparateTags("tr", lines)){
			if(ParseData.removeHTML(temp).size()==16){//Normal Classes
				courseList.add(new Course(ParseData.removeHTML(temp).toArray(new String[15])));
			}
			else if(ParseData.removeHTML(temp).size()==14){//Classes with undetermined location and time information
				String[] tempCourse = ParseData.removeHTML(temp).toArray(new String[14]);
				String[] course = {tempCourse[0],tempCourse[1],tempCourse[2],tempCourse[3],tempCourse[4],tempCourse[5],"TBD","TBD","TBD","TBD",tempCourse[8],tempCourse[9],tempCourse[10],tempCourse[11],tempCourse[12],tempCourse[13]};
				courseList.add(new Course(course));
			}
			else if(ParseData.removeHTML(temp).size()==15){//Lines with additional information about other classes
				Course c = courseList.get(courseList.size()-1);
				String days = ParseData.removeHTML(temp).get(5).replaceAll("&nbsp;"," ");
				c.addDays(days.replace("&nbsp;", " "));
				String startTime = ParseData.removeHTML(temp).get(6).replaceAll("&nbsp;"," ");
				c.setStartTime(SimpleTime.parseSimpleTime(startTime.replace("&nbsp;", " ").replace("-", "")));
				String endTime = ParseData.removeHTML(temp).get(7).replaceAll("&nbsp;"," ");
				c.setEndTime(SimpleTime.parseSimpleTime(endTime.replace("&nbsp;", " ").replace("-", "")));
				String loc = ParseData.removeHTML(temp).get(8).replaceAll("&nbsp;"," ");
				c.setLocation(c.getLocation()+" and "+loc);
			}
		}
		return courseList;
	}
	
	public static ArrayList<Course> createCoursesFromUrl(String url){
		try {
			return CourseBuilder.createCoursesFromRawData(ParseData.toLines(ParseData.getHTML(url)));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
